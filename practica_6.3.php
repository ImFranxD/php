<!Doctype html>
<head>
	<title>Practica 6.3</title>
	<?php
	$host='localhost';
	$username='root';
	$nombreBD='universidad';
	$password='';
	try{
		$base_de_datos=new PDO("mysql:host=$host;dbname=$nombreBD;charset=utf8;port=3306", $username, $password);
	}catch(PDOException $e){
		echo "Ocurrio un error con la base de datos: ".$e->getMessage();
	}
	?>
</head>
<body>
	<?php
	echo "<table border='3'>";
	echo "<td>DNI</td>";
	echo "<td>Nombre</td>";
	echo "<td>Apellido 1</td>";
	echo "<td>Apellido 2</td>";
	echo "<td>Localidad</td>";
	echo "<td>Fecha Nacimiento</td>";
	$busqueda=$base_de_datos->prepare("SELECT * FROM alumno");
	$busqueda->execute();
	$resultado=$busqueda->fetchALL(PDO::FETCH_ASSOC);
	foreach ($resultado as $res) {
		echo "<tr>";
		echo "<td>".$res["DNI"]."</td>";
		echo "<td>".$res["NOMBRE"]."</td>";
		echo "<td>".$res["APELLIDO_1"]."</td>";
		echo "<td>".$res["APELLIDO_2"]."</td>";
		echo "<td>".$res["LOCALIDAD"]."</td>";
		echo "<td>".$res["FECHA_NACIMIENTO"]."</td>";
		echo "</tr>";
	}
	?>
</body>